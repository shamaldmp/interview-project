import React, { useState, useEffect } from "react";
import raw from "./graphData.json";
import BootstrapTable from "react-bootstrap-table-next";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import filterFactory, {
  dateFilter,
  numberFilter,
  textFilter,
} from "react-bootstrap-table2-filter";
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css";

function DataList() {
  const [userList, setuserList] = useState([]);

  const columns = [
    { dataField: "name", text: "Title", sort: true, filter: textFilter() },
    {
      dataField: "indexData.lastUpdate",
      text: "Last Updated",
      sort: true,
      filter: dateFilter(),
    },
    { dataField: "selfEvaluation", text: "Confidence", sort: true },
    { dataField: "totalTimeS", text: "Time", sort: true },
    {
      dataField: "indexData.percentFillerWords",
      text: "%FW",
      sort: true,
      filter: numberFilter(),
    },
    { dataField: "indexData.wpm", text: "WPM", sort: true },
  ];

  const pagination = paginationFactory({
    page: 1,
    sizePerPage: 5,
    lastPageText: ">>",
    firstPageText: "<<",
    nextPageText: ">",
    prePageText: "<",
    showTotal: true,
    alwaysShowAllBtns: true,
    onPageChange: function (page, sizePerPage) {
      console.log("page", page);
      console.log("sizePerPage", sizePerPage);
    },
    onSizePerPageChange: function (page, sizePerPage) {
      console.log("page", page);
      console.log("sizePerPage", sizePerPage);
    },
  });

  useEffect(() => {
    const posts = raw.rows.map((obj) => obj);
    setuserList(posts);
  }, []);
  return (
    <div>
      <BootstrapTable
        bootstrap4
        keyField="indexData.lastUpdate"
        columns={columns}
        data={userList}
        pagination={pagination}
        filter={filterFactory()}
        bordered={ false }
      />
    </div>
  );
}

export default DataList;
