import React, { useState } from "react";
import "./App.css";
import DataList from "./DataGrid";
import Switch from "./switch";
import InterviewRows from "./interviewPresentation";

function App() {
  const [value, setValue] = useState(false);
  return (
    <div className="App">
      
      <div style={{ display: !value ? "block" : "none" }}>
        <DataList />
      </div>
      <div style={{ display: value ? "block" : "none" }}>
        <InterviewRows />
      </div>
      <Switch isOn={value} handleToggle={() => setValue(!value)} />
    </div>
  );
}

export default App;
